//! # The modules for the needle programme.
//!
//! The needle programme is based on the Needleman Wunch. There is a separate module for the
//! algorithm itself (`algo`) and for the subsitiution matrix (`subs`).
// TODO use a adjacency graph to return multiple alignments.
// TODO make sure that the borders are initialised well.

/// # Module for the Needlenan-Wunch algorithm.
///
/// In this module, the Needleman-Wunch algorithm is implemented.
pub mod algo {
    use itertools::Itertools;
    #[cfg(windows)]
    /// Windows line ending
    const LINE_ENDING: &'static str = "\r\n";
    #[cfg(not(windows))]
    /// The unix line ending.
    const LINE_ENDING: &'static str = "\n";

    use std::{fmt, ops::Add};

    /// # Modulo operator
    ///
    /// Modulo that handles negative numbers, works the same as Python's `%`.
    ///
    /// eg: `(a + b).modulo(c)`
    /// ## Reference
    /// Written by [Ideaman42](https://stackoverflow.com/a/41422009).
    pub trait ModuloSignedExt {
        fn modulo(
            &self,
            n: Self,
        ) -> Self;
    }
    macro_rules! modulo_signed_ext_impl {
        ($($t:ty)*) => ($(
            impl ModuloSignedExt for $t {
                #[inline]
                fn modulo(&self, n: Self) -> Self {
                    (self % n + n) % n
                }
            }
        )*)
    }
    modulo_signed_ext_impl! { i8 i16 i32 i64 usize}

    use super::subs::SubsitutionMatrix;

    /// # Directions to move in the traceback matrix.
    ///
    /// This enum will be used for the stage where the alignment is constructed.
    // TODO Make the encapsulation proper.
    #[derive(Clone, Debug, PartialEq)]
    pub enum Directions {
        /// When there is a gap in Y.
        Up,
        /// When there is a gap in X.
        Left,
        /// When there is no gap.
        Diagonal,
        /// When the direction is not yet set.
        Unknown,
    }

    impl Directions {
        /// # Get the next coordinate.
        ///
        /// If i and j are the current coordinates, the next coordinates are:
        /// * Up: i-1, j
        /// * Diagonal: i-1, j-1
        /// * Left: i, j-1.
        ///
        /// When the direction is `Unknown`, the method will panic.
        fn value(&self) -> (i64, i64) {
            match *self {
                Self::Up => (-1, 0),
                Self::Diagonal => (-1, -1),
                Self::Left => (0, -1),
                Self::Unknown => {
                    panic!("The traceback matrix must be filled!")
                }
            }
        }
    }

    /// # An alignent result from a Needleman-Wunch alignment.
    #[derive(Debug, PartialEq, Eq)]
    pub struct NeedlemanWunchAlignment {
        pub(super) x: String,
        pub(super) y: String,
        pub(super) gap_score: i64,
        pub(super) end_gap: i64,
        pub(super) sub: SubsitutionMatrix,
        pub(super) score: i64,
    }

    /// # Converts a string to a `Vec` where each item is a wrapped string.
    fn wrap_string(
        x: &String,
        n: usize,
    ) -> Vec<String> {
        let mut res: Vec<String> = Vec::new();
        let mut res_part = String::new();
        let mut i: usize = 0;
        for ch in x.chars() {
            if i == n {
                res_part.push(ch);
                res.push(res_part.clone());
                res_part.clear();
                i = 0;
            } else {
                res_part.push(ch);
                i += 1;
            }
        }
        res.push(res_part.clone());
        res
    }

    impl fmt::Display for NeedlemanWunchAlignment {
        /// # Display an Needleman-Wunch alignment.
        fn fmt(
            &self,
            f: &mut fmt::Formatter,
        ) -> fmt::Result {
            let line_number: usize = 51;
            let alignment = crate::needle::subs::SubsitutionMatrix::get_alignment_string_symbol(&self.x, &self.y);
            // formed_string is a vector of vectors with lenght 3. The first element being x, second being alignment string, third is y.
            let mut formed_strings: Vec<Vec<String>> = Vec::new();
            let mut out = String::new();
            out += &format!("Score: {}\n", self.score);
            for alg in alignment.iter() {
                formed_strings.push(wrap_string(alg, line_number));
            }
            let len = formed_strings[0].len();
            for i in 0..len {
                for j in (0..3) {
                    out += &(formed_strings[j][i].clone() + &"\n".to_string());
                }
                out += "\n";
            }
            fmt::Display::fmt(&out, f)
        }
    }

    /// # A Needleman Wunch alignment.
    ///
    pub struct NeedlemanWunch {
        /// Sequence x.
        x: String,
        /// Sequence y.
        y: String,
        /// Score to use for gaps.
        gap_score: i64,
        /// Score to use for end gaps only.
        end_gap: i64,
        /// The subsitition matrix.
        sub: SubsitutionMatrix,
        /// The traceback matrix with rows being the scores for y, and the columns being scores for x.
        pub(super) trace: Vec<Vec<i64>>,
        /// The direction matrix with rows being the scores for y, and the columns being scores for x.
        pub(super) direction: Vec<Vec<Directions>>,
    }

    impl NeedlemanWunch {
        /// # Getting the Needleman Wunch alignment of two (bio) strings.
        ///
        /// ## Parameters
        /// * `sequence_x` : `String` - The first sequence.
        /// * `sequence_y` : `String` - The second sequence.
        /// * `end_gap` : `i64` - The end gap score.
        /// * `sub` : `SubstitutionMatrix` - The subsititution matrix to use.
        /// Note: the `bio_type` of the substitution matrix must be the same as the
        /// bio_type of the sequence string, meaning that a `Protein` substitution matrix
        /// must be used for a string containing _only_ protein characters.
        pub fn new(
            x: String,
            y: String,
            end_gap: i64,
            gap_score: i64,
            sub: SubsitutionMatrix,
        ) -> NeedlemanWunch {
            // Calculating the matrix.

            let trace: Vec<Vec<i64>> =
                (0..x.len() + 1).map(|_| vec![0; y.len() + 1]).collect();
            let direction =
                vec![vec![Directions::Unknown; y.len() + 1]; x.len() + 1];
            let mut out = NeedlemanWunch {
                x,
                y,
                gap_score,
                end_gap,
                sub,
                trace,
                direction,
            };

            if out.end_gap > 0 {
                panic!("A end_gap of {} was selected, only negative values are allowed.", out.end_gap)
            }
            if out.gap_score > 0 {
                panic!("A gap score of {} was selected, only negative values are allowed.", out.gap_score)
            }
            out.initialise();
            out
        }

        /// # Initialise the borders of the `trace` matrix.
        ///
        /// The self parameter end gap cost is taken into account.
        pub fn initialise(&mut self) {
            let trace_range = (0..self.trace.len()).rev();
            // let use_end_weight: bool = true;
            for i in trace_range {
                let trace_i_range = (0..self.trace[i].len()).rev();
                for j in trace_i_range {
                    let val = self.trace[i][j];
                    let i_n = (i as i64) * self.end_gap;
                    let j_n = (j as i64) * self.end_gap;
                    if i == 0 {
                        self.direction[i][j] = Directions::Left;
                        self.trace[i][j] = val.add(&j_n);
                    }
                    if j == 0 {
                        self.direction[i][j] = Directions::Up;
                        self.trace[i][j] = val.add(&i_n);
                    }
                }
                self.direction[0][0] = Directions::Diagonal;
            }
        }

        /// # Set the traceback matrices
        ///
        /// Set the score matrix and the traceback matrixes at the same time. It uses the formula
        /// ```text
        ///      /  S_(i-1, j-1) + s(x_i, y_j)  diagonal
        /// max |   S_(i-1, j) + gap    Up
        ///      \  S_(i, j-1) + gap    left
        /// ```
        pub fn set_traceback(&mut self) {
            // TODO make the final border actually set correctly.
            let x_chars: Vec<char> = self.x.chars().collect();
            let y_chars: Vec<char> = self.y.chars().collect();
            let (xlen, ylen) = (x_chars.len(), y_chars.len());

            let trace = &mut self.trace;
            let direction = &mut self.direction;

            for (x_index, x_char) in x_chars.iter().enumerate().take(xlen) {
                for (y_index, y_char) in y_chars.iter().enumerate().take(ylen)
                {
                    // Getting the score
                    let score = self.sub.get_score(*x_char, *y_char);

                    // Calculating the possible matches
                    let options = match (x_index, y_index) {
                        (x_len, _) => [
                            trace[x_index][y_index] + score,
                            trace[x_index][y_index + 1] + self.end_gap,
                            trace[x_index + 1][y_index] + self.gap_score,
                        ],
                        (_, y_len) => [
                            trace[x_index][y_index] + score,
                            trace[x_index][y_index + 1] + self.gap_score,
                            trace[x_index + 1][y_index] + self.end_gap,
                        ],
                        (x_len, y_len) => [
                            trace[x_index][y_index] + score,
                            trace[x_index][y_index + 1] + self.gap_score,
                            trace[x_index + 1][y_index] + self.gap_score,
                        ],
                        (_, _) => [
                            trace[x_index][y_index] + score,
                            trace[x_index][y_index + 1] + self.gap_score,
                            trace[x_index + 1][y_index] + self.gap_score,
                        ],
                    };

                    // Getting the best match
                    let max_score = *options.iter().max().unwrap();
                    let choice =
                        options.iter().position(|&x| x == max_score).unwrap();

                    // Setting the matrix
                    trace[x_index + 1][y_index + 1] = max_score;
                    let direction_choice = match choice {
                        0 => Directions::Diagonal,
                        1 => Directions::Up,
                        2 => Directions::Left,
                        _ => Directions::Unknown,
                    };
                    direction[x_index + 1][y_index + 1] = direction_choice;
                }
            }
        }

        /// # Getting the traceback vector.
        ///
        /// # Returns
        ///  Two `Vec`s for backtrace step `i` in `N`.
        ///
        /// - `Vec<(usize, usize)>` -- Coordinates at each traceback step. Starting at the bottom right of the `trace`.
        /// - `Vec<Directions>` -- Represents the direction taken at each backtrace step.
        pub fn get_traceback(
            &self
        ) -> (Vec<(usize, usize)>, Vec<&Directions>) {
            // Here be the traceback
            let xlen = self.x.len();
            let ylen = self.y.len();
            let mut coord = (xlen, ylen);
            let max_len = ylen.max(xlen);
            let mut directions: Vec<&Directions> = Vec::with_capacity(max_len);
            let mut indeces: Vec<(usize, usize)> = Vec::with_capacity(max_len);
            while coord != (0_usize, 0_usize) {
                let (i, j) = coord;
                let current_direction = &self.direction[i][j];
                let next_coordinate = current_direction.value();
                indeces.push(coord);
                // Set the new coordinate.
                coord = (
                    (i as i64 + next_coordinate.0) as usize,
                    (j as i64 + next_coordinate.1) as usize,
                );
                directions.push(current_direction);
            }
            (indeces, directions)
        }

        /// # Construct the alignment from a traceback coordinates and directions Vec.
        ///
        /// # Parameters
        /// - `indeces`: `Vec<(usize, usize)>` -- The coordinates of the `trace` matrix for each entry in `directions`.
        /// - `directions`: `Vec<Directions>` -- The direction taken at each step of the traceback.
        pub fn construct_alignment(
            &self,
            indices: Vec<(usize, usize)>,
            directions: Vec<&Directions>,
        ) -> NeedlemanWunchAlignment {
            let mut x: Vec<char> = Vec::with_capacity(directions.len());
            let mut y: Vec<char> = Vec::with_capacity(directions.len());
            let x_chars: Vec<char> = self.x.chars().collect();
            let y_chars: Vec<char> = self.y.chars().collect();
            let indices: Vec<&(usize, usize)> = indices.iter().collect();
            for (index, direction) in directions.iter().enumerate() {
                let (i, j) = (indices[index].0, indices[index].1);
                let x_ch = x_chars[i - 1];
                let y_ch = y_chars[j - 1];
                match direction {
                    Directions::Diagonal => {
                        x.push(x_ch);
                        y.push(y_ch);
                    }
                    Directions::Up => {
                        y.push('-');
                        x.push(x_ch);
                    }
                    Directions::Left => {
                        x.push('-');
                        y.push(y_ch);
                    }
                    Directions::Unknown => {
                        panic!("Trace matrix not filled correctly.")
                    }
                };
            }
            x.reverse();
            y.reverse();
            let x: Vec<String> =
                x.into_iter().map(|x_i| x_i.to_string()).collect();
            let y: Vec<String> =
                y.into_iter().map(|y_i| y_i.to_string()).collect();
            NeedlemanWunchAlignment {
                x: x.join(""),
                y: y.join(""),
                gap_score: self.gap_score,
                end_gap: self.end_gap,
                sub: self.sub.clone(),
                score: self.trace[self.x.len()][self.y.len()],
            }
        }
    }

    /// Printing the traceback matrix.
    impl fmt::Debug for NeedlemanWunch {
        fn fmt(
            &self,
            f: &mut fmt::Formatter,
        ) -> fmt::Result {
            let mut out = String::from("\n");
            for row in &self.trace {
                for col in row {
                    out += &format!("{col:>4} ");
                }
                out += "\n";
            }
            write!(f, "{}", out)
        }
    }
}

/// # The test cases for the Needleman-Wunch algorithm.
#[cfg(test)]
mod test_algo {
    use crate::needle::algo::{
        Directions, NeedlemanWunch, NeedlemanWunchAlignment,
    };

    use crate::needle::subs::*;
    use std::path::Path;
    use std::vec;

    /// # Test whether the `trace` and `direction` matrices were correctly set.
    fn check_trace_mats(
        _x: String,
        _y: String,
        trace: &Vec<Vec<i64>>,
        dirs: &Vec<Vec<Directions>>,
        gap_score: i64,
        _end_gap: i64,
        _sub: SubsitutionMatrix,
    ) -> () {
        for (row_index, row) in trace.iter().enumerate() {
            for (col_index, _) in row.iter().enumerate() {
                // Checking each cell of the trace mat.
                let coords: (usize, usize) = (row_index, col_index);
                let dir = dirs[row_index][col_index].clone();
                let tr = trace[row_index][col_index] as usize;
                match coords {
                    (0, 0) => {
                        // First value
                        assert_eq!(tr, 0);
                        assert_eq!(dir, Directions::Diagonal);
                    }
                    (0, _) => {
                        // i = 0, top row region
                        assert_eq!(
                            tr as i64,
                            (col_index as i64) * (gap_score)
                        );
                        assert_eq!(dir, Directions::Left);
                    }
                    (_, 0) => {
                        // j = 0, left column region
                        assert_eq!(
                            tr as i64,
                            (row_index as i64) * (gap_score)
                        );
                        assert_eq!(dir, Directions::Up);
                    }
                    (_, _) => {
                        // i, j are within the matrix
                        let options = vec![
                            trace[row_index - 1][col_index - 1],
                            trace[row_index - 1][col_index],
                            trace[row_index][col_index - 1],
                        ];
                        let pointer = match dir {
                            Directions::Diagonal => {
                                (row_index - 1, col_index - 1)
                            }
                            Directions::Left => (row_index - 1, col_index),
                            Directions::Up => (row_index, col_index - 1),
                            _ => panic!("All directions must be known"),
                        };
                        let index = match &dir {
                            Directions::Diagonal => 0,
                            Directions::Left => 1,
                            Directions::Up => 2,
                            _ => panic!(),
                        };
                        assert_eq!(
                            options.iter().nth(index).unwrap().clone(),
                            trace[pointer.0][pointer.1].clone()
                        );
                    }
                }
            }
        }
    }

    #[test]
    fn test_init_end_gap() {
        let x = "THISLINE".to_owned();
        let y = "ISALIGNED".to_owned();
        let file: &Path = Path::new("assets/blosum_62.txt");
        let blosum = import_protein_substitution_matrix(file);
        let nw = NeedlemanWunch::new(x.clone(), y.clone(), 0, -1, blosum);
        // Test if intialisation at 0 works
        assert_eq!(nw.direction[0][0], Directions::Diagonal);
        for i in 1..x.len() + 1 {
            assert_eq!(nw.trace[i][0], 0);
            assert_eq!(nw.direction[i][0], Directions::Up);
        }
        for j in 1..y.len() + 1 {
            assert_eq!(nw.trace[0][j], 0);
            assert_eq!(nw.direction[0][j], Directions::Left);
        }
    }

    #[test]
    #[should_panic]
    fn test_init_bad() {
        let file: &Path = Path::new("assets/blosum_62.txt");
        let blosum = import_protein_substitution_matrix(file);
        let mut nw = NeedlemanWunch::new(
            "AATTRR".to_string(),
            "LLFFW".to_string(),
            -1,
            1,
            blosum,
        );
        // Positive gap scores are not allowed.
        nw.initialise();
        nw.set_traceback()
    }

    #[test]
    fn test_alignment_book() {
        let x = "THISLINE".to_string();
        let y = "ISALIGNED".to_string();
        let end_gap = -8;
        let gap_score = end_gap;

        let file: &Path = Path::new("assets/blosum_62.txt");
        let blosum = import_protein_substitution_matrix(file);
        let mut alg = NeedlemanWunch::new(
            x.clone(),
            y.clone(),
            end_gap,
            gap_score,
            blosum.clone(),
        );
        alg.set_traceback();
        let trace = &alg.trace;
        assert_eq!({ *trace.last().unwrap().last().unwrap() }, -4);
        check_trace_mats(
            x,
            y,
            &trace,
            &alg.direction,
            gap_score,
            end_gap,
            blosum,
        );
    }

    #[test]
    fn test_trace_mech() {
        println!("test traceback");
        let x = "THISLINE".to_string();
        let y = "ISALIGNED".to_string();
        let end_gap = -8;
        let gap_score = end_gap;

        let file: &Path = Path::new("assets/blosum_62.txt");
        let blosum = import_protein_substitution_matrix(file);
        let mut alg = NeedlemanWunch::new(
            x.clone(),
            y.clone(),
            end_gap,
            gap_score,
            blosum.clone(),
        );
        alg.set_traceback();
        let result = alg.get_traceback();
        let alignment = alg.construct_alignment(result.0, result.1);
        let correct = NeedlemanWunchAlignment {
            x: "THISLINE-".to_string(),
            y: "ISALIGNED".to_string(),
            gap_score: -8,
            end_gap: -8,
            sub: blosum,
            score: alg.trace[x.len()][y.len()],
        };
        assert_eq!(alignment, correct);
    }

    #[test]
    fn test_long_output() {
        let x: String = "MGLLCSRSRHHTEDTDENAQAAEIERRIEQEAKAEKHIRKLLLLGAGESGKSTIFKQASSDKRKIIKLLFQTGFDEGELKSYVPVIHANVLLLGAYQTIKLLHDGTKEFAQNETDPAKYTLSSENMAIGEKLSEIGARLDYPRLTKDLAEGIETLWNDPAIQETCSRGNELQVPDCTKYLMENLKRLSDVNYIPTKEDVLYARVRTTGVVEIQFSPVGENKKSGEVYRLFDVGGQRNERRKWIHLFEGVTAVIFCAAISEYDQTLFEDEQKNRMMETKELFDWVLKQPCFEKTSIMLFLNKFDIFEKKVLDVPLNVCEWFRDYQPVSSGKQEIEHAYEFVKKKFEELYYQNTAPDRVDRVFKIYRTTALDQKLVKKTFKLVDETLRRRNLLEAGLLMGLLCSRSRHHTEDTDENAQAAEIERRIEQEAKAEKHIRKLLLLGAGESGKSTIFKQASSDKRKIIKLLFQTGFDEGELKSYVPVIHANVYQTIKLLHDGTKEFAQNETDPAKYTLSSENMAIGEKLSEIGARLDYPRLTKDLAEGIETLWNDPAIQETCSRGNELQVPDCTKYLMENLKRLSDVNYIPTKEDVLYARVRTTGVVEIQFSPVGENKKSGEVYRLFDVGGQRNERRKWIHLFEGVTAVIFCAAISEYDQTLFEDEQKNRMMETKELFDWVLKQPCFEKTSIMLFLNKFDIFEKKVLDVPLNVCEWFRDYQPVSSGKQEIEHAYEFVKKKFEELYYQNTAPDRVDRVFKIYRTTALDQKLVKKTFKLVDETLRRRNLLEAGLLMGLLCSRSRHHTEDTDENAQAAEIERRIEQEAKAEKHIRKLLLLGAGESGKSTIFKQASSDKRKIIKLLFQTGFDEGELKSYVPVIHANVYQTIKLLHDGTKEFAQNETDPAKYTLSSENMAIGEKLSEIGARLDYPRLTKDLAEGIETLWNDPAIQETCSRGNELQVPDCTKYLMENLKRLSDVNYIPTKEDVLYARVRTTGVVEIQFSPVGENKKSGEVYRLFDVGGQRNERRKWIHLFEGVTAVIFCAAISEYDQTLFEDEQKNRMMETKELFDWVLKQPCFEKTSIMLFLNKFDIFEKKVLDVPLNVCEWFRDYQPVSSGKQEIEHAYEFVKKKFEELYYQNTAPDRVDRVFKIYRTTALDQKLVKKTFKLVDETLRRRNLLEAGLL".to_string();
        let y: String = "MGLLCSRSRHHTEDTDENAQAAEIERRIEQEAKAEKHIRKLLLLGAGESGKSTIFKQALLLGASSDKRKIIKLLFQTGFDLLLGAEGELKSYVLLLGAPVIHANLLLGAVYQTIKLLHDGTKEFAQNETDPAKYTLSSENMAIGEKLSEIGARLDYPRLTKDLAEGIETLWNDPAIQETCSRGNELQVPDCTKYLMENLKRLSDVNYIPTKEDVLYARVRTTGVVEIQFSPVGENKKSGEVYRLFDVGGQRNERRKWIHLFEGVTAVIFCAAISEYDQTLFEDEQKNRMMETKELFDWVLKQPCFEKTSIMLFLNKFDIFEKKVLDVPLNVCEWFRDYQPVSSGKQEIEHAYEFVKKKFEELYYQNTAPDRVDRVFKIYRTTALDQKLVKKTFKLVDETLRRRNLLEAGLMGLLCSRSRHHTEDTDENAQAAEIERRIEQEAKAEKHIRKLLLLGAGESGKSTIFKQASSDKRKIIKLLFQTGFDEGELKSYVPVIHANVYQTIKLLHDGTKEFAQNETDPAKYTLSSENMAIGEKLSEIGARLDYPRLTKDLAEGIETLWNDPAIQETCSRGNELQVPDCTKYLMENLKRLSDVNYIPTKEDVLYARVRTTGVVEIQFSPVGENKKSGEVYRLFDVGGQRNERRKWIHLFEGVTAVIFCAAISEYDQTLFEDEQKNRMMETKELFDWVLKQPCFEKTSIMLFLNKFDIFEKKVLDVPLNVCEWFRDYQPVSSGKQEIEHAYEFVKKKFEELYYQNTAPDRVDRVFKIYRTTALDQKLVKKTFKLVDETLRRRNLLEAGLMGLLCSRSRHHTEDTDENAQAAEIERRIEQEAKAEKHIRKLLLLGAGESGKSTIFKQASSDKRKIIKLLFQTGFDEGELKSYVPVIHANVYQTIKLLHDGTKEFAQNETDPAKYTLSSENMAIGEKLSEIGARLDYPRLTKDLAEGIETLWNDPAIQETCSRGNELQVPDCTKYLMENLKRLSDVNYIPTKEDVLYARVRTTGVVEIQFSPVGENKKSGEVYRLFDVGGQRNERRKWIHLFEGVTAVIFCAAISEYDQTLFEDEQKNRMMETKELFDWVLKQPCFEKTSIMLFLNKFDIFEKKVLDVPLNVCEWFRDYQPVSSGKQEIEHAYEFVETLRRRNLLEAKKKFEELYYQNTAPRTTALDQKLVKKTFKLVDETLRRRNLLEA".to_string();

        let file: &Path = Path::new("assets/blosum_62.txt");
        let blosum = import_protein_substitution_matrix(file);
        for i in 1..1 {
            let end_gap = -1 * i;
            let gap_score = end_gap;
            let mut alignment = NeedlemanWunch::new(
                x.clone(),
                y.clone(),
                end_gap,
                gap_score,
                blosum.clone(),
            );
            alignment.set_traceback();
            let res = alignment.get_traceback();
            alignment.construct_alignment(res.0, res.1);
        }
    }
    // let result = alignment.get_traceback();
    // let alignment = alignment.construct_alignment(result.0, result.1);
    // println!("{}", alignment);

    #[test]
    fn test_alignmnent_print() {
        let x: String = "MGLLCSRAAATTTTTTTTTTTTTTTTTTTAAMGLLCSRAAATTTTTTTTTTTTTTTTTTTAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAASRHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAASRH".to_string();
        let y: String = "MGLLCSRSAABFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFMGLLCSRSAABFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAAAAAAAABBBAAAAAAAAAAAAARHHTEDTDFFFFFFFFFFFFFFFFFAAAAAAAABBBAAAAAAAAAAAAARHHTEDTD".to_string();
        let file: &Path = Path::new("assets/blosum_62.txt");
        let blosum = import_protein_substitution_matrix(file);
        let end_gap = -8;
        let gap_score = end_gap;
        let mut alignment = NeedlemanWunch::new(
            x.clone(),
            y.clone(),
            end_gap,
            gap_score,
            blosum.clone(),
        );
        alignment.set_traceback();
        let res = alignment.get_traceback();
        let alg = alignment.construct_alignment(res.0, res.1);
        println!("{}", alg);
    }

    #[test]
    fn test_wierd_scores() {
        let x: String = "MGLLCSRAAATTTTTTTTTTTTTTTTTTTAAMGLLCSRAAATTTTTTTTTTTTTTTTTTTAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAASRHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAASRH".to_string();
        let y: String = "MGLLCSRSAABFFFFFFFFFFFFFFFFFFFFAFFFFFFFFFFFMGLLCSRSAABFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAAAAAAAABBBAAAAAAAAAAAAARHHTEDTDFFFFFFFFFFFFFFFFFAAAAAAAABBBAAAAAAABAAAAAAAAAAAAARHHTEDTDAAAAAARHHTEDTDSSS".to_string();
        let file: &Path = Path::new("assets/blosum_62.txt");
        let blosum = import_protein_substitution_matrix(file);
        let end_gap = -1;
        let gap_score = -8;
        let gap_score = end_gap;
        let mut alignment = NeedlemanWunch::new(
            x.clone(),
            y.clone(),
            end_gap,
            gap_score,
            blosum.clone(),
        );
        alignment.set_traceback();
        let res = alignment.get_traceback();
        let alg = alignment.construct_alignment(res.0, res.1);
        println!("{}", alg);
    }
}

/// # A module for subsitution matrices
///
/// In this module, there are facilities to handle subsitiution matrices. It is possible to import
/// a subsitiution from a text file, and it is possible to get the score for difference between two
/// chars. Additionaly, it is possible to get the difference symbols according to the [alignment formats](https://www.ebi.ac.uk/seqdb/confluence/display/JDSAT/Pairwise+Sequence+Alignment+Output+Examples) format.
// TODO Make the matrix persistent.
// Implement for DNA.
// TODO make the matrix downloadable.
pub mod subs {
    use crate::sequence::bio::{IsAminoAcid, IsDna, SequenceType};
    use std::collections::HashMap;
    use std::fs::read_to_string;
    use std::path::*;
    use std::str::FromStr;

    /// # Struct for SubsitutionMatrix
    ///
    /// In the SubsitutionMatrix there are 4 fields:
    /// 1. `name` indicating what the matrix is called.
    /// 2. `bio_type` indicating for what sequence the matrix is for.
    /// 3. `matrix` Holding the data for the subsitiution matrix. Private field.
    /// 4. `order` a hashmap with the indexing information of the subsitiution matrix. Private field.
    #[derive(Debug, PartialEq, Eq, Clone)]
    pub struct SubsitutionMatrix {
        pub name: String,
        pub bio_type: SequenceType,
        matrix: Vec<Vec<i64>>,
        order: HashMap<String, usize>,
    }

    impl SubsitutionMatrix {
        /// # Construct a SubsitutionMatrix object
        ///
        /// ## Parameters
        /// * `name` : The name of the subsitiution matrix.
        /// * `bio_type` : The characters that the subsitiution may accept.
        /// * `matrix` : The subsitiution scores.
        /// * `order` : The indexing information.
        pub fn new(
            name: String,
            bio_type: SequenceType,
            matrix: Vec<Vec<i64>>,
            order: HashMap<String, usize>,
        ) -> SubsitutionMatrix {
            SubsitutionMatrix {
                name,
                bio_type,
                matrix,
                order,
            }
        }

        /// # Get the subsitiution score.
        ///
        /// ## Parameters
        /// * `char_x` : One character.
        /// * `char_y` : The other character.
        ///
        /// ## Returns
        /// The subsitiution score
        ///
        /// ## Note
        /// The input characters must fit the `bio_type` field.
        pub fn get_score(
            &self,
            char_x: char,
            char_y: char,
        ) -> i64 {
            // Checking input char class
            let correct: bool = match self.bio_type {
                // TODO in this which statement, you can return the acceptable score.
                SequenceType::Dna => {
                    char_x.is_simple_iupac_dna()
                        && char_y.is_simple_iupac_dna()
                }
                SequenceType::Protein => {
                    char_x.is_standard_amino_acid()
                        && char_y.is_standard_amino_acid()
                } // There is no other option.
            };
            if !correct {
                panic!("Bad subsitution.");
            } else {
                // here we get the score
                let (i, j) = (
                    self.order[&char_x.to_string()],
                    self.order[&char_y.to_string()],
                );
                self.matrix[i][j]
            }
        }

        /// # Output the alignment string in the pair format.
        /// ## Parameters
        /// - `x` : `&str` --- The x sequence.
        /// - `y` : `&str` --- The y sequence.
        /// # Panic
        /// When the sequences are not of the same length.
        /// ## Returns
        /// The symbols that are output are:
        /// * ` `: Gap.
        /// * `|`: Match
        /// * `.`: Mismatch
        /// * `*`: Mismatch, but same meaning.
        // TODO Finish return comment.
        pub(crate) fn get_alignment_string_symbol(
            x: &str,
            y: &str,
        ) -> Vec<String> {
            let x_chars: Vec<char> = x.chars().collect();
            let y_chars: Vec<char> = y.chars().collect();
            let xlen = x_chars.len();
            let mut out: Vec<char> = Vec::with_capacity(xlen);

            if xlen == y_chars.len() {
                for pos in x_chars.iter().zip(y_chars.iter()) {
                    if pos.0 == pos.1 {
                        // full match.
                        out.push('|');
                    } else if pos.0 == &'-' || pos.1 == &'-' {
                        out.push(' ');
                    } else if pos.0 != pos.1 {
                        out.push(*pos.1);
                    }
                }
            } else {
                panic!("X and Y must be of equal length!")
            }
            let out = String::from_iter(out);
            vec![
                String::from_str(x).unwrap(),
                out,
                String::from_str(y).unwrap(),
            ]
        }
    }

    /// # Import a protein substituion matrix from a text file
    ///
    /// The file must be from NCBI, `#` Indicating comment lines. Row and
    /// column _must_ start with the character of the substituion in question.
    /// ## Parameters
    /// * `file` : `&Path` - A reference to a path pointing to a subsitution matrix.
    /// ## Returns
    /// `SubsitutionMatrix` - where the `name` field is the first line of the `file`.
    /// ## Data format
    /// Here is an example of an accepted data format:
    /// ```text
    /// # http://www.ncbi.nlm.nih.gov/Class/FieldGuide/BLOSUM62.txt
    /// #  Matrix made by matblas from blosum62.iij
    /// #  * column uses minimum score
    /// #  BLOSUM Clustered Scoring Matrix in 1/2 Bit Units
    /// #  Blocks Database = /data/blocks_5.0/blocks.dat
    /// #  Cluster Percentage: >= 62
    /// #  Entropy =   0.6979, Expected =  -0.5209
    /// A  R  N  D  C  Q  E  G  H  I  L  K  M  F  P  S  T  W  Y  V  B  Z  X  *
    ///    A  4 -1 -2 -2  0 -1 -1  0 -2 -1 -1 -1 -1 -2 -1  1  0 -3 -2  0 -2 -1  0 -4
    ///    R -1  5  0 -2 -3  1  0 -2  0 -3 -2  2 -1 -3 -2 -1 -1 -3 -2 -3 -1  0 -1 -4
    ///    N -2  0  6  1 -3  0  0  0  1 -3 -3  0 -2 -3 -2  1  0 -4 -2 -3  3  0 -1 -4
    ///    D -2 -2  1  6 -3  0  2 -1 -1 -3 -4 -1 -3 -3 -1  0 -1 -4 -3 -3  4  1 -1 -4
    ///    C  0 -3 -3 -3  9 -3 -4 -3 -3 -1 -1 -3 -1 -2 -3 -1 -1 -2 -2 -1 -3 -3 -2 -4
    ///    Q -1  1  0  0 -3  5  2 -2  0 -3 -2  1  0 -3 -1  0 -1 -2 -1 -2  0  3 -1 -4
    ///    E -1  0  0  2 -4  2  5 -2  0 -3 -3  1 -2 -3 -1  0 -1 -3 -2 -2  1  4 -1 -4
    ///    G  0 -2  0 -1 -3 -2 -2  6 -2 -4 -4 -2 -3 -3 -2  0 -2 -2 -3 -3 -1 -2 -1 -4
    ///    H -2  0  1 -1 -3  0  0 -2  8 -3 -3 -1 -2 -1 -2 -1 -2 -2  2 -3  0  0 -1 -4
    ///    I -1 -3 -3 -3 -1 -3 -3 -4 -3  4  2 -3  1  0 -3 -2 -1 -3 -1  3 -3 -3 -1 -4
    ///    L -1 -2 -3 -4 -1 -2 -3 -4 -3  2  4 -2  2  0 -3 -2 -1 -2 -1  1 -4 -3 -1 -4
    ///    K -1  2  0 -1 -3  1  1 -2 -1 -3 -2  5 -1 -3 -1  0 -1 -3 -2 -2  0  1 -1 -4
    ///    M -1 -1 -2 -3 -1  0 -2 -3 -2  1  2 -1  5  0 -2 -1 -1 -1 -1  1 -3 -1 -1 -4
    ///    F -2 -3 -3 -3 -2 -3 -3 -3 -1  0  0 -3  0  6 -4 -2 -2  1  3 -1 -3 -3 -1 -4
    ///    P -1 -2 -2 -1 -3 -1 -1 -2 -2 -3 -3 -1 -2 -4  7 -1 -1 -4 -3 -2 -2 -1 -2 -4
    ///    S  1 -1  1  0 -1  0  0  0 -1 -2 -2  0 -1 -2 -1  4  1 -3 -2 -2  0  0  0 -4
    ///    T  0 -1  0 -1 -1 -1 -1 -2 -2 -1 -1 -1 -1 -2 -1  1  5 -2 -2  0 -1 -1  0 -4
    ///    W -3 -3 -4 -4 -2 -2 -3 -2 -2 -3 -2 -3 -1  1 -4 -3 -2 11  2 -3 -4 -3 -2 -4
    ///    Y -2 -2 -2 -3 -2 -1 -2 -3  2 -1 -1 -2 -1  3 -3 -2 -2  2  7 -1 -3 -2 -1 -4
    ///    V  0 -3 -3 -3 -1 -2 -2 -3 -3  3  1 -2  1 -1 -2 -2  0 -3 -1  4 -3 -2 -1 -4
    ///    B -2 -1  3  4 -3  0  1 -1  0 -3 -4  0 -3 -3 -2  0 -1 -4 -3 -3  4  1 -1 -4
    ///    Z -1  0  0  1 -3  3  4 -2  0 -3 -3  1 -1 -3 -1  0 -1 -3 -2 -2  1  4 -1 -4
    ///    X  0 -1 -1 -1 -2 -1 -1 -1 -1 -1 -1 -1 -1 -1 -2  0  0 -2 -1 -1 -1 -1 -1 -4
    ///    * -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4 -4  1
    /// ```
    pub fn import_protein_substitution_matrix(
        file: &Path
    ) -> SubsitutionMatrix {
        let mut scores: Vec<Vec<i64>> = Vec::new();
        let mut order: HashMap<String, usize> = HashMap::new();
        let mut header: Vec<String> = Vec::new();
        // A parser for protein subsitiution matrices.
        for line in read_to_string(file).unwrap_or("# ".to_string()).lines() {
            if line.starts_with('#') || line.is_empty() {
                // header region
                let edit = line.replace("#  ", "");
                let edit = edit.replace("# ", "");
                header.push(edit);
            } else {
                // amino acid region
                let mut data = line.split(' ').collect::<Vec<&str>>();
                data.retain(|&x| !x.is_empty());
                if data[0]
                    .chars()
                    .next()
                    .unwrap_or('.')
                    .is_standard_amino_acid()
                {
                    // The row is amino acid!
                    if data[1].chars().next().unwrap().is_standard_amino_acid()
                    {
                        // Now we are in the order region
                        for (i, amino) in data.into_iter().enumerate() {
                            order.insert((*amino).to_string(), i);
                        }
                    } else {
                        data.remove(0);
                        let scores_lines: Vec<i64> = data
                            .iter()
                            .map(|&s| s.parse::<i64>().unwrap())
                            .collect();
                        scores.push(scores_lines);
                    }
                }
            }
        }
        SubsitutionMatrix::new(
            header[0].clone(),
            SequenceType::Protein,
            scores,
            order,
        )
    }
}

#[cfg(test)]
mod test_subs {

    use crate::needle::subs::*;
    use std::path::*;

    #[test]
    fn test_read_blosum_matrix_working_scores() {
        let file: &Path = Path::new("assets/blosum_62.txt");
        let blosum = import_protein_substitution_matrix(file);
        assert_eq!(blosum.get_score('A', 'A'), 4);
        assert_eq!(blosum.get_score('A', 'R'), -1);
        assert_eq!(blosum.get_score('A', '*'), -4);
        assert_eq!(blosum.get_score('*', '*'), 1);
        assert_eq!(
            blosum.name,
            "http://www.ncbi.nlm.nih.gov/Class/FieldGuide/BLOSUM62.txt"
        );
    }

    #[test]
    #[should_panic]
    fn test_read_blosum_matrix_impossible_scores() {
        let file: &Path = Path::new("assets/blosum_62.txt");
        let blosum = import_protein_substitution_matrix(file);
        // z is not a protein.
        assert_eq!(blosum.get_score('z', 'A'), 4);
    }

    #[test]
    fn test_make_alignment_string() {
        let file: &Path = Path::new("assets/blosum_62.txt");
        let blosum = import_protein_substitution_matrix(file);
        // z is not a protein.
        let x = "QQQQQWWTWW---TTTTT";
        let y = "QQQQQWWWWWEEETTTTT";
        let alignment =
            crate::needle::subs::SubsitutionMatrix::get_alignment_string_symbol(x , y);
        let correct =
            "QQQQQWWTWW---TTTTT\n|||||||W||   |||||\nQQQQQWWWWWEEETTTTT"
                .to_string();
        assert_eq!(alignment.join("\n"), correct);
    }
}
