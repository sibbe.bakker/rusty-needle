/// A module with helper functions.
pub mod utils {
    use crate::sequence::bio;
    use crate::sequence::bio::Length;
    use crate::sequence::bio::Reverse;
    use std::fs::read_to_string;
    use std::path::*;

    /// # Verbose print.
    ///
    /// A function to only print when the programme acts verbosely as indicated
    /// by the `verbose` parameter.
    ///
    /// ## Arguments
    /// * `text : String` - The text string to be printed to standard out.
    /// * `verbose : &bool` - Whether to print the `text` to the standard out
    /// (`true`) or not (`false`).
    pub fn vbprint(
        text: String,
        verbose: &bool,
    ) {
        if *verbose {
            println!("{}", text)
        }
    }

    /// # Reading in a DNA object from a file.
    ///
    /// ## Arguments
    /// * `file name : &Path` - The system path to the file to be read in.
    pub fn read_lines(filename: &Path) -> bio::Dna {
        let mut result = Vec::new();
        for line in read_to_string(filename).unwrap().lines() {
            result.push(line.to_string())
        }
        let seq = result.join("");
        bio::Dna::new(seq)
    }

    /// # Calculating the analysis and getting the result on the screen.
    ///
    /// ## Arguments
    /// * `mut data : dna::Dna` - A Dna object of which a summary is to be
    /// printed.
    /// ## Description
    /// This function summarises the following.
    /// * Sequence length.
    /// * THe GC content, accurate to two decimals.
    /// * The DNA sequence printed as a reverse complement.
    pub fn print_summary(mut dna: bio::Dna) {
        println!("The length of the sequence is {} bp", dna.length());
        println!(
            "The GC content  of the sequence is {:.2}%",
            dna.calculate_gc_percentage()
        );
        dna.reverse();
        dna.complement();
        println!("This is the reverse complement of the sequence:");
        println!("{}", dna);
    }
}
