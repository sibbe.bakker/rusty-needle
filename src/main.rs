mod helpers;
mod needle;
mod sequence;

use clap::Parser;

use needle::{algo::NeedlemanWunch, subs::import_protein_substitution_matrix};
use std::{env, path::*};
extern crate argparse;
#[macro_use]
extern crate assert_float_eq;

/// A programme to do a Needleman-Wunch alignment
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// String A; Protein
    #[arg(short, long)]
    a: String,

    /// String B; Protein
    #[arg(short, long)]
    b: String,

    /// negative number, the gap score.
    #[arg(short, long, allow_hyphen_values = true)]
    gap_score: i64,

    /// Negative number, the end gap score.
    #[arg(short, long, allow_hyphen_values = true)]
    end_gap: i64,
}
const PROGRAMME_NAME: &str = "Rusty-Needle";

fn main() {
    env::set_var("RUST_BACKTRACE", "1");
    let args = Args::parse();
    let file: &Path = Path::new("assets/blosum_62.txt");
    let sub = import_protein_substitution_matrix(file);
    let mut align =
        NeedlemanWunch::new(args.a, args.b, args.end_gap, args.gap_score, sub);
    align.initialise();
    align.set_traceback();
    let tr = align.get_traceback();
    let res = align.construct_alignment(tr.0, tr.1);
    println!("{}", res);
}
