/// Dna related structs and methods
///
/// # Parameters
/// * `sequence : String` - A string of DNA or protein dependent on type used.
/// Must contain only simple IUAPC bases. If this is not true, the programme
/// will panic.
pub mod bio {
    use std::collections::{HashMap, HashSet};
    use std::fmt;

    /// Types of accepted and implemented sequence types.
    #[derive(Clone, Debug, PartialEq, Eq)]
    pub enum SequenceType {
        Dna,
        Protein,
    }

    /// A sequence of character.
    pub trait Sequence {
        // TODO add: is valid, len and reverse. (low prio)
        // TODO use a default trait implementation for get sequence. Or via a custom validator.

        /// # Getter of the private sequence attribute
        fn get_sequence(&self) -> &str;

        /// # Setter of the private sequence attribute
        fn set_sequence(
            &mut self,
            sequence: &String,
        ) -> ();
        // fn length(&self) -> i32 {
        //     let seq = self.get_sequence();
        //     seq.len() as i32
        // }
        // fn reverse(&mut self) -> () {
        //     let reversed = &self
        //         .get_sequence()
        //         .to_uppercase()
        //         .chars()
        //         .rev()
        //         .collect::<String>();
        //     self.set_sequence(&reversed.to_string());
        // }
    }

    /// Reverse the sequence in place.
    pub trait Reverse {
        fn reverse(&mut self) -> ();
    }

    impl<T: Sequence> Reverse for T {
        /// # Determine the reverse of a sequence.
        ///
        /// Reverses a sequence in place.
        fn reverse(&mut self) -> () {
            let reversed = &self
                .get_sequence()
                .to_uppercase()
                .chars()
                .rev()
                .collect::<String>();
            self.set_sequence(&reversed.to_string());
        }
    }

    /// Get sequence length of a sequence.
    pub trait Length {
        fn length(&self) -> i32;
    }

    impl<T: Sequence> Length for T {
        /// # Determine the length of a sequence.
        ///
        /// # Returns
        /// The length of the sequence.
        fn length(&self) -> i32 {
            let seq = self.get_sequence();
            seq.len() as i32
        }
    }

    /// A display implementation trait for the dna struct.
    impl fmt::Display for Dna {
        fn fmt(
            &self,
            f: &mut fmt::Formatter,
        ) -> fmt::Result {
            write!(f, "{}", self.get_sequence())
        }
    }

    /// A display implementation trait for the protein struct.
    impl fmt::Display for Protein {
        fn fmt(
            &self,
            f: &mut fmt::Formatter,
        ) -> fmt::Result {
            write!(f, "{}", self.get_sequence())
        }
    }

    /// A trait to determine if a character is a valid DNA nucleotide.
    pub trait IsDna {
        ///
        /// # Returns
        ///
        /// `true` if the character is a valid DNA nucleotide, `false`
        /// otherwise.
        fn is_simple_iupac_dna(&self) -> bool;
    }

    impl IsDna for char {
        /// Determines if a character is a valid DNA nucleotide, i.e., 'A',
        /// 'C', 'G', or 'T'.
        ///
        /// # Returns
        ///
        /// `true` if the character is a valid DNA nucleotide, `false`
        /// otherwise.
        fn is_simple_iupac_dna(&self) -> bool {
            matches!(self.to_ascii_uppercase(), 'A' | 'C' | 'G' | 'T')
        }
    }

    /// A trait to determine if a character is a valid standard amino acid.
    pub trait IsAminoAcid {
        ///
        /// # Returns
        ///
        /// `true` if the character is a valid standard amino acid residue,
        /// `false` otherwise.
        fn is_standard_amino_acid(&self) -> bool;
    }

    impl IsAminoAcid for char {
        /// Determines if a character is a valid standard amino acid residue.
        ///
        /// # Returns
        ///
        /// `true` if the character is a valid standard amino acid residue,
        /// `false` otherwise.
        fn is_standard_amino_acid(&self) -> bool {
            // A hash set is used to lower the look-up cost.
            let valid_residues: HashSet<char> = [
                'A', 'R', 'N', 'D', 'C', 'Q', 'E', 'G', 'H', 'I', 'L', 'K',
                'M', 'F', 'P', 'S', 'T', 'W', 'Y', 'V', '*', 'B', 'Z', 'X',
            ]
            .iter()
            .cloned()
            .collect();
            valid_residues.contains(&self.to_ascii_uppercase())
        }
    }

    /// The struct for Dna
    pub struct Dna {
        pub sequence: String,
    }

    /// Struct for protein
    pub struct Protein {
        pub sequence: String,
    }

    impl Protein {
        /// # Construct a `Protein` struct.
        ///
        /// # Parameters
        /// * `sequence : String` - A string of Protein. Must contain only simple
        /// IUAPC residues. If this is not true, the programme will panic.
        pub fn new(sequence: String) -> Protein {
            Self::is_valid_sequence(&sequence);
            Self { sequence }
        }

        /// # Check whether a sequence is a valid protein sequence.
        ///
        /// ## Parameters
        ///  * `sequence` : `&str` - The protein sequence as a reference of a string.
        ///
        /// ## Note
        /// If the sequence is not a protein sequence, the thread that made the
        /// sequence will panic.
        fn is_valid_sequence(sequence: &str) {
            let validity =
                sequence.chars().all(|c| c.is_standard_amino_acid());
            if !validity {
                panic!("This is not a valid protein sequence")
            }
        }
    }

    impl Sequence for Protein {
        fn get_sequence(&self) -> &str {
            &self.sequence
        }
        fn set_sequence(
            &mut self,
            sequence: &String,
        ) -> () {
            self.sequence = sequence.clone();
            let validity =
                sequence.chars().all(|c| c.is_standard_amino_acid());
            if !validity {
                panic!("This is not a valid protein sequence")
            }
        }
    }

    impl Dna {
        /// # Construct a `Dna` struct.
        /// ## Parameters
        /// * `sequence : String` - A string of DNA. Must contain only simple
        /// IUAPC bases. If this is not true, the programme will panic.
        pub fn new(sequence: String) -> Dna {
            Self::is_valid_sequence(&sequence);
            Self { sequence }
        }

        /// # Check whether a sequence is a valid dna sequence.
        ///
        /// ## Parameters
        ///  * `sequence` : `&str` - The DNA sequence as a reference of a string.
        ///
        /// ## Note
        ///
        /// If the sequence is not a DNA sequence, the thread that made the
        /// sequence will panic.
        fn is_valid_sequence(sequence: &str) {
            let validity = sequence.chars().all(|c| c.is_simple_iupac_dna());
            if !validity {
                panic!("This is not a valid Dna sequence.");
            }
        }

        /// # Determine the GC percentage of a DNA sequence.
        ///
        /// # Returns
        /// The `CG` percentage.
        pub fn calculate_gc_percentage(&self) -> f64 {
            let sequence_len = self.length();
            let letter_counts: HashMap<char, i64> = self
                .sequence
                .to_lowercase()
                .chars()
                .fold(HashMap::new(), |mut map, c| {
                    *map.entry(c).or_insert(0) += 1;
                    map
                });
            let g_count = *letter_counts.get(&'g').unwrap_or(&0);
            let c_count = *letter_counts.get(&'c').unwrap_or(&0);
            ((c_count + g_count) as f64 / sequence_len as f64) * 100.0
        }

        /// #  Determine the complement of the dna.
        ///
        /// Sets complement of the DNA in place.
        pub fn complement(&mut self) {
            let complement_map = HashMap::from([
                ('A', 'T'),
                ('T', 'A'),
                ('C', 'G'),
                ('G', 'C'),
                ('N', 'N'),
            ]);
            let complement = &self
                .sequence
                .to_uppercase()
                .chars()
                .map(|base| complement_map.get(&base).unwrap_or(&'X'))
                .collect::<String>();
            self.sequence = complement.to_string();
        }
    }

    impl Sequence for Dna {
        fn get_sequence(&self) -> &str {
            &self.sequence
        }

        fn set_sequence(
            &mut self,
            sequence: &String,
        ) -> () {
            self.sequence = sequence.clone();
            let validity = sequence.chars().all(|c| c.is_simple_iupac_dna());
            if !validity {
                panic!("This is not a valid Dna sequence")
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::bio::*;
    #[test]
    #[should_panic]
    fn test_dna_validator() {
        let seq = String::from("QGATVMQWERgasfWERThsdlkahdhdkASDAS");
        let mut invalid_dna = Dna::new(seq);
        invalid_dna.reverse()
    }

    #[test]
    fn test_dna_validator_no_panic() {
        let seq = String::from("ATGC");
        let mut invalid_dna = Dna::new(seq);
        invalid_dna.reverse()
    }

    #[test]
    fn test_gc_content() {
        let expect = 59.46;
        let seq = String::from("AAACACCACACAGGGCGCGAGCGACCGATGACTGACT");
        let dna = Dna::new(seq);
        let gc_percentage = dna.calculate_gc_percentage();
        assert_float_absolute_eq!(gc_percentage, expect, 0.1);
    }

    #[test]
    fn test_len_dna() {
        let expect = 4;
        let seq = String::from("AAAA");
        let dna = Dna::new(seq);
        let len = dna.length();
        assert_eq!(len, expect);
    }

    #[test]
    fn test_reverse_dna() {
        let expect = String::from("GCTA");
        let seq = String::from("ATCG");
        let mut dna = Dna::new(seq);
        dna.reverse();
        assert_eq!(dna.sequence, expect);
    }

    #[test]
    #[should_panic]
    fn test_protein_validator() {
        let seq = String::from("B1q`j");
        let mut invalid_protein = Protein::new(seq);
        invalid_protein.reverse()
    }

    #[test]
    fn test_protein_validator_no_panic() {
        let seq = String::from("ARNDCEQGHILKMFPSTWYV");
        let mut invalid_protein = Protein::new(seq);
        invalid_protein.reverse()
    }

    #[test]
    fn test_len_protein() {
        let expect = 10;
        let seq = String::from("AMMAAVVATG");
        let protein = Protein::new(seq);
        let len = protein.length();
        assert_eq!(len, expect);
    }

    #[test]
    fn test_reverse_protein() {
        let expect = String::from("MVTAGQ");
        let seq = String::from("QGATVM");
        let mut protein = Protein::new(seq);
        protein.reverse();
        print!("{}", protein.sequence);
        assert_eq!(protein.sequence, expect);
    }
}
